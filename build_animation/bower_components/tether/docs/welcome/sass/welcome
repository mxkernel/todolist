/* line 3, welcome.sass */
html, body {
  height: 100%; }

/* line 6, welcome.sass */
body {
  margin: 0;
  font-family: "proxima-nova", "Helvetica Neue", sans-serif; }

/* line 10, welcome.sass */
.button {
  display: inline-block;
  border: 2px solid #333;
  color: #333;
  padding: 1em 1.25em;
  font-weight: 500;
  text-transform: uppercase;
  letter-spacing: 3px;
  text-decoration: none;
  cursor: pointer;
  width: 140px;
  font-size: 0.8em;
  line-height: 1.3em;
  text-align: center; }

/* line 25, welcome.sass */
.tether-element.tether-theme-arrows-dark .tether-content {
  padding: 1em;
  font-size: 1.1em; }
  /* line 29, welcome.sass */
  .tether-element.tether-theme-arrows-dark .tether-content .button {
    border-color: #fff;
    color: #fff;
    width: 170px;
    pointer-events: all; }

/* line 35, welcome.sass */
.mobile-copy {
  display: none; }
  @media (max-width: 568px) {
    /* line 35, welcome.sass */
    .mobile-copy {
      display: block; } }

/* line 41, welcome.sass */
.button.dark {
  background: #333;
  color: #fff; }

/* line 45, welcome.sass */
.hero-wrap {
  height: 100%;
  overflow: hidden; }

/* line 49, welcome.sass */
table.showcase {
  height: 100%;
  width: 100%;
  position: relative; }
  /* line 54, welcome.sass */
  table.showcase:after {
    content: "";
    display: block;
    position: absolute;
    left: 0;
    right: 0;
    bottom: 20px;
    margin: auto;
    height: 0;
    width: 0;
    border-width: 18px;
    border-style: solid;
    border-color: transparent;
    border-top-color: rgba(0, 0, 0, 0.2); }
  /* line 69, welcome.sass */
  table.showcase.no-next-arrow:after {
    display: none; }
  /* line 72, welcome.sass */
  table.showcase .showcase-inner {
    margin: 40px auto 60px;
    padding: 10px; }
    /* line 76, welcome.sass */
    table.showcase .showcase-inner h1 {
      font-size: 50px;
      text-align: center;
      font-weight: 300; }
      @media (max-width: 567px) {
        /* line 76, welcome.sass */
        table.showcase .showcase-inner h1 {
          font-size: 40px; } }
    /* line 84, welcome.sass */
    table.showcase .showcase-inner h2 {
      font-size: 24px;
      text-align: center;
      font-weight: 300;
      margin: 1em 0 1em; }
      @media (max-width: 567px) {
        /* line 84, welcome.sass */
        table.showcase .showcase-inner h2 {
          font-size: 14px; } }
    /* line 93, welcome.sass */
    table.showcase .showcase-inner p {
      text-align: center; }
  /* line 96, welcome.sass */
  table.showcase.hero {
    text-align: center; }
    /* line 99, welcome.sass */
    table.showcase.hero .tether-target-demo {
      display: inline-block;
      vertical-align: middle;
      *vertical-align: auto;
      *zoom: 1;
      *display: inline;
      border: 2px dotted #000;
      margin: 5rem auto;
      padding: 5rem; }
      @media (max-width: 567px) {
        /* line 99, welcome.sass */
        table.showcase.hero .tether-target-demo {
          padding: 1rem; } }
  /* line 108, welcome.sass */
  table.showcase.share {
    background: #f3f3f3; }
  /* line 113, welcome.sass */
  table.showcase.projects-showcase .showcase-inner .projects-list {
    width: 80%;
    max-width: 1200px;
    margin: 0 auto; }
    /* line 118, welcome.sass */
    table.showcase.projects-showcase .showcase-inner .projects-list .project {
      color: inherit;
      text-decoration: none;
      position: relative;
      width: 50%;
      float: left;
      text-align: center;
      margin-bottom: 2rem; }
      /* line 127, welcome.sass */
      table.showcase.projects-showcase .showcase-inner .projects-list .project:nth-child(odd) {
        clear: left; }
    /* line 130, welcome.sass */
    table.showcase.projects-showcase .showcase-inner .projects-list .os-icon {
      width: 8rem;
      height: 8rem;
      margin-bottom: 1rem;
      background-size: 100%; }
    /* line 136, welcome.sass */
    table.showcase.projects-showcase .showcase-inner .projects-list h1 {
      font-size: 2.5rem; }
    /* line 139, welcome.sass */
    table.showcase.projects-showcase .showcase-inner .projects-list p {
      font-size: 1.3rem; }
  /* line 142, welcome.sass */
  table.showcase.browser-demo {
    background-image: linear-gradient(top left, #723362 0%, #9d223c 100%);
    background-color: #9d223c;
    position: absolute;
    top: 100%; }
    /* line 148, welcome.sass */
    table.showcase.browser-demo.fixed {
      position: fixed;
      top: 0;
      bottom: 0;
      left: 0;
      right: 0;
      z-index: 1; }
      /* line 156, welcome.sass */
      table.showcase.browser-demo.fixed .browser-demo-inner {
        transition: width 2s ease-in-out, height 2s ease-in-out; }
      /* line 161, welcome.sass */
      table.showcase.browser-demo.fixed[data-section="what"] {
        box-shadow: 0 0 0 0; }
      /* line 166, welcome.sass */
      table.showcase.browser-demo.fixed[data-section="why"] .browser-demo-inner {
        width: 70%; }
      /* line 171, welcome.sass */
      table.showcase.browser-demo.fixed[data-section="outro"] .showcase-inner {
        pointer-events: all; }
    /* line 174, welcome.sass */
    table.showcase.browser-demo .showcase-inner {
      pointer-events: none;
      position: absolute;
      left: 10%;
      right: 40%;
      top: 220px;
      bottom: 120px;
      margin: 0;
      padding: 0; }
      @media (max-width: 567px) {
        /* line 174, welcome.sass */
        table.showcase.browser-demo .showcase-inner {
          bottom: 90px;
          top: 180px; } }
    /* line 188, welcome.sass */
    table.showcase.browser-demo .browser-demo-inner {
      height: 100%;
      width: 100%; }
    /* line 192, welcome.sass */
    table.showcase.browser-demo .section-copy {
      transition: opacity 0.5s ease-in-out, top 0.5s ease-in-out;
      opacity: 0;
      position: absolute;
      top: 0;
      position: absolute;
      height: 200px;
      color: #fff;
      text-align: center;
      width: 100%; }
      /* line 203, welcome.sass */
      table.showcase.browser-demo .section-copy.active {
        opacity: 1;
        top: -150px; }
        @media (max-width: 567px) {
          /* line 203, welcome.sass */
          table.showcase.browser-demo .section-copy.active {
            top: -130px; } }
      /* line 210, welcome.sass */
      table.showcase.browser-demo .section-copy h2 {
        font-size: 40px;
        font-weight: bold;
        line-height: 1;
        margin: 25px 0 15px; }
        @media (max-width: 567px) {
          /* line 210, welcome.sass */
          table.showcase.browser-demo .section-copy h2 {
            font-size: 30px; } }
    /* line 219, welcome.sass */
    table.showcase.browser-demo .browser-window {
      border-radius: 4px;
      background: #fff;
      position: relative;
      height: 100%;
      width: 100%;
      max-width: 1200px;
      margin: 0 auto; }
      /* line 228, welcome.sass */
      table.showcase.browser-demo .browser-window .browser-titlebar {
        position: absolute;
        top: 0;
        left: 0;
        right: 0;
        border-bottom: 1px solid #eee;
        height: 55px; }
        /* line 236, welcome.sass */
        table.showcase.browser-demo .browser-window .browser-titlebar .browser-dots {
          padding: 16px; }
          /* line 239, welcome.sass */
          table.showcase.browser-demo .browser-window .browser-titlebar .browser-dots b {
            display: inline-block;
            vertical-align: middle;
            *vertical-align: auto;
            *zoom: 1;
            *display: inline;
            border-radius: 50%;
            width: 10px;
            height: 10px;
            margin-right: 7px;
            background: rgba(0, 0, 0, 0.1); }
      /* line 247, welcome.sass */
      table.showcase.browser-demo .browser-window .browser-frame {
        position: absolute;
        top: 55px;
        left: 0;
        right: 0;
        bottom: 0; }
        /* line 254, welcome.sass */
        table.showcase.browser-demo .browser-window .browser-frame iframe {
          border-radius: 0 0 4px 4px;
          border: 0;
          width: 100%;
          height: 100%; }
  /* line 262, welcome.sass */
  table.showcase.browser-demo-section .section-scroll-copy {
    position: relative;
    z-index: 10;
    color: #fff;
    width: 100%;
    font-size: 22px; }
    /* line 269, welcome.sass */
    table.showcase.browser-demo-section .section-scroll-copy .section-scroll-copy-inner {
      position: absolute;
      z-index: 10;
      color: #fff;
      right: 10%;
      width: 23%; }
      /* line 276, welcome.sass */
      table.showcase.browser-demo-section .section-scroll-copy .section-scroll-copy-inner a {
        color: inherit; }
      /* line 279, welcome.sass */
      table.showcase.browser-demo-section .section-scroll-copy .section-scroll-copy-inner .example-paragraph {
        border-radius: 4px;
        background: #000;
        padding: 1rem; }

/* line 284, welcome.sass */
.browser-content {
  display: none; }
